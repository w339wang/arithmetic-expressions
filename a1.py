import matplotlib.pyplot as plt
import numpy as np
import torch

import pyro
import pyro.infer
import pyro.optim
import pyro.distributions as dist
import pdb

safe_div = lambda x, y : 0 if y == 0 else  x / y

def productions(id):
    # print(id)
    R = pyro.sample('R'+id, dist.Categorical(probs=torch.tensor([1]*10)))
    if R < 3:
        c = pyro.sample('c'+id, dist.Categorical(probs=torch.tensor([1]*10)))
        return lambda x: c.type(torch.float)
    if R < 6:
        return lambda x : x
    if R < 7:
        g = productions(id+'+l')
        h = productions(id+'+r')
        return lambda x : g(x) + h(x)
    if R < 8:
        g = productions(id+'-l')
        h = productions(id+'-r')
        return lambda x : g(x) - h(x)
    if R < 9:

        g = productions(id+'*l')
        h = productions(id+'*r')
        return lambda x : g(x) * h(x)

    g = productions(id+'/l')
    h = productions(id+'/r')
    return lambda x : safe_div(g(x), h(x))
def model(obs, xs, x):
    f = productions('')
    for i, ob in enumerate(obs):
        pyro.sample('ob'+str(i), dist.Normal(f(xs[i]), 1e-2), obs=torch.tensor(ob))
    # pyro.sample('f2', dist.Normal(f(2), 1e-2), obs=torch.tensor(3))
    # pyro.sample('f3', dist.Normal(f(3), 1e-2), obs=torch.tensor(1))
    return pyro.sample('fx', dist.Normal(f(x), 1e-2))

x = 4
xs =[1, 2, 3]
f = lambda x: -2*x + 7
obs = [f(x) for x in xs]

model(obs, xs, x)

is_posterior = pyro.infer.Importance(model, num_samples=10000).run(obs, xs, x)
is_marginal = pyro.infer.EmpiricalMarginal(is_posterior, "fx")
is_samples = torch.stack([is_marginal() for _ in range(10000)]).numpy()
plt.hist(is_samples)
plt.show()
